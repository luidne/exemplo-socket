package exemplo.socket.servidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Luidne
 */
public class ThreadServidor extends Thread {

    
    private static Logger log = Logger.getLogger(ThreadServidor.class.getName());
    
    private Socket conexao;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private String mensagemRecebida;
    private String mensagemParaEnviar;

    public ThreadServidor(Socket socket) {
        this.conexao = socket;
        this.setName(socket.getInetAddress().getHostAddress());
        PropertyConfigurator.configure("F:\\Luidne\\Documents\\NetBeansProjects\\exemplo-socket-2\\src\\log4j.properties");
    }

    @Override
    public void run() {
        try {
            // objetos que permitem controlar fluxo de comunicação
            this.out = new ObjectOutputStream(this.conexao.getOutputStream());
            this.out.flush();
            this.in = new ObjectInputStream(this.conexao.getInputStream());

            Servidor.CLIENTES_CONECTADOS++;

            // primeiramente, espera-se pelo nome do cliente
            this.mensagemRecebida = this.lerMensagem();

            while (this.mensagemRecebida != null && !(this.mensagemRecebida.isEmpty() || this.mensagemRecebida.trim().equals("sair"))) {
                this.mensagemParaEnviar = this.calcular(this.mensagemRecebida);
                this.enviarMensagem(this.mensagemParaEnviar);

                System.out.println();
                this.mensagemRecebida = this.lerMensagem();
            }
        } catch (Exception ex) {
            log.error(ex + " : Thread[ "+this.getName()+"]");
        } finally {
            // Uma vez que o cliente enviou linha em branco, retira-se fluxo de saída do vetor de CLIENTES_CONECTADOS e fecha-se conexão.
            System.out.println("Cliente: " + this.conexao.getInetAddress().getHostName() + " se desconectou.");
            Servidor.CLIENTES_CONECTADOS--;

            try {
                this.out.close();
                this.in.close();
                this.conexao.close();
            } catch (Exception ex) {
                log.error(ex + " : Thread[ "+this.getName()+"]");
            }

        }
    }

    public String calcular(String mensagem) {
        StringTokenizer st = new StringTokenizer(mensagem, " ");
        String resultado = "";

        try {
            double aux = Double.parseDouble(st.nextToken());

            while (st.hasMoreTokens()) {
                aux /= Double.parseDouble(st.nextToken());
            }
            resultado = aux + "";
            if (resultado.equals("NaN") || resultado.equals("Infinity")) {
                return "Não é possível dividir.";
            }
        } catch (Exception ex) {
        }

        return resultado;
    }

    public void enviarMensagem(String mensagem) {
        try {
            this.out.writeObject(mensagem);
            this.out.flush();
            System.out.println("servidor# " + mensagem);
        } catch (IOException ex) {
            log.error(ex + " : Thread[ "+this.getName()+"]");
        }
    }

    public String lerMensagem() {
        String mensagem = "";

        try {
            mensagem = (String) this.in.readObject();
            System.out.println("cliente# " + mensagem);
        } catch (Exception ex) {
            log.error(ex + " : Thread[ "+this.getName()+"]");
        } finally {
            return mensagem;
        }
    }
}
