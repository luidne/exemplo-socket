package exemplo.socket.servidor;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * @author Luídne Mota
 */
public class DNS {

    private ServerSocket providerSocket;
    private Socket connection;
    public static HashMap<String, String> IP_SERVIDORES;
    
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Servidor.class.getName()); // Logger do aplicativo

    public DNS() {
        PropertyConfigurator.configure("F:\\Luidne\\Documents\\NetBeansProjects\\exemplo-socket-2\\src\\log4j.properties");
        log.info("DNS iniciado.");
        
        IP_SERVIDORES = new HashMap<>();

        IP_SERVIDORES.put("somar", "10.50.0.251:2004");
        IP_SERVIDORES.put("subtrair", "10.50.1.249:2002");
        IP_SERVIDORES.put("multiplicar", "10.50.0.75:2002");
        IP_SERVIDORES.put("dividir", "localhost:2001");
    }

    void iniciar() {
        try {
            //1. Criando o servidor socket
            this.providerSocket = new ServerSocket(2005);
            ThreadDNS.CLIENTES_CONECTADOS = new Vector();

            //2. Esperando pela conexão
            System.out.println("::::::::::: Servidor DNS :::::::::::::::::::");
            while (true) {
                System.out.println("Esperando por uma conexão...");
                this.connection = this.providerSocket.accept();

                Thread thread = new ThreadDNS(this.connection);
                thread.start();

                System.out.println("Conexão recebida de: " + this.connection.getInetAddress().getHostName());

                System.out.println();
            }
        } catch (IOException ex) {
            log.error(ex);
        } finally {
            //4: Fechando a conexão
            try {
                this.providerSocket.close();
            } catch (IOException ex) {
                log.error(ex);
            }
        }
    }

    public static void main(String[] args) {

        new DNS().iniciar();

    }
}
