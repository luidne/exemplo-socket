package exemplo.socket.servidor;

import java.io.*;
import java.net.*;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


/**
 * @author Luídne Mota
 */
public class Servidor {

    private ServerSocket providerSocket;
    private Socket connection;
    
    public static final int MAX_CONEXAO = 1;
    public static int CLIENTES_CONECTADOS = 0;
    
    private static Logger log = Logger.getLogger(Servidor.class.getName());

    public Servidor() {
        PropertyConfigurator.configure("F:\\Luidne\\Documents\\NetBeansProjects\\exemplo-socket-2\\src\\log4j.properties");
        log.info("Servidor iniciado.");
    }

    public void iniciar() {
        try {
            //1. Criando o servidor socket
            this.providerSocket = new ServerSocket(2001);
            
            System.out.println(":::::::::::::::: Servidor de divisão ::::::::::::");
            
            while(true){
                if(CLIENTES_CONECTADOS < MAX_CONEXAO){
                    System.out.println("Esperando por uma conexão...");

                    //2. Esperando pela conexão
                    this.connection = this.providerSocket.accept();
                    Thread thread = new ThreadServidor(this.connection);
                    thread.start();
                    System.out.println("Conexão recebida de: " + this.connection.getInetAddress().getHostName());
                    if(!(CLIENTES_CONECTADOS < MAX_CONEXAO)){
                        log.info("Serivdor de operação lotado.");
                    }
                }
            }
        } catch (Exception ex) {
            log.error(ex);
        } finally {
            try {
                this.providerSocket.close();
            } catch (IOException ex) {
                log.error(ex);
            }
        }
    }

    public static void main(String[] args) {

        new Servidor().iniciar();

    }
}
