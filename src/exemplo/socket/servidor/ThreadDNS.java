package exemplo.socket.servidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Luidne
 */
public class ThreadDNS extends Thread {

    public static Vector CLIENTES_CONECTADOS;
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Servidor.class.getName()); // Logger do aplicativo
    // socket deste cliente
    private Socket conexao;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private String mensagemRecebida;
    private String mensagemParaEnviar;

    public ThreadDNS(Socket socket) {
        PropertyConfigurator.configure("F:\\Luidne\\Documents\\NetBeansProjects\\exemplo-socket-2\\src\\log4j.properties");

        this.conexao = socket;
    }

    @Override
    public void run() {
        try {
            // objetos que permitem controlar fluxo de comunicação
            this.in = new ObjectInputStream(this.conexao.getInputStream());
            this.out = new ObjectOutputStream(this.conexao.getOutputStream());

            // primeiramente, espera-se pelo nome do cliente
            this.mensagemRecebida = this.lerMensagem();

            CLIENTES_CONECTADOS.add(this.out);

            while (this.mensagemRecebida != null && !(this.mensagemRecebida.isEmpty() || this.mensagemRecebida.trim().equals("sair"))) {

                this.mensagemParaEnviar = DNS.IP_SERVIDORES.get(this.mensagemRecebida);

                this.enviarMensagem(this.mensagemParaEnviar);

                System.out.println();
                this.mensagemRecebida = this.lerMensagem();
            }
            // Uma vez que o cliente enviou linha em branco, retira-se fluxo de saída do vetor de CLIENTES_CONECTADOS e fecha-se conexão.
            System.out.println("Cliente: " + this.conexao.getInetAddress().getHostName() + " se desconectou.");
            CLIENTES_CONECTADOS.remove(this.out);
            this.conexao.close();
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public String calcular(String mensagem) {
        StringTokenizer st = new StringTokenizer(mensagem, " ");
        String resultado = "";

        try {
            double aux = Double.parseDouble(st.nextToken());

            while (st.hasMoreTokens()) {
                aux /= Double.parseDouble(st.nextToken());
            }
            resultado = aux + "";
            if (resultado.equals("NaN") || resultado.equals("Infinity")) {
                return "Não é possível dividir.";
            }
        } catch (Exception ex) {
        }

        return resultado;
    }

    public void enviarMensagem(String mensagem) {
        try {
            this.out.writeObject(mensagem);
            this.out.flush();
            System.out.println("servidor# " + mensagem);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public String lerMensagem() {
        String mensagem = "";

        try {
            mensagem = (String) this.in.readObject();
            System.out.println("cliente# " + mensagem);
        } catch (Exception ex) {
            log.error(ex);
        } finally {
            return mensagem;
        }
    }
}
