package exemplo.socket.cliente;

import exemplo.socket.servidor.Servidor;
import java.io.*;
import java.net.*;
import java.util.StringTokenizer;
import org.apache.log4j.PropertyConfigurator;

/**
 * @author Luídne Mota
 */
public class Cliente {

    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    
    public static final int TIME_OUT = 5000; // 5 segundos
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Servidor.class.getName()); // Logger do aplicativo

    public Cliente(String host) throws Exception {
        PropertyConfigurator.configure("F:\\Luidne\\Documents\\NetBeansProjects\\exemplo-socket-2\\src\\log4j.properties");
        
        StringTokenizer st = new StringTokenizer(host, ":");

        //1. criando o socket para conectar no servidor
        this.requestSocket = new Socket(st.nextToken(), Integer.parseInt(st.nextToken()));
        this.requestSocket.setSoTimeout(TIME_OUT);
        try{
            //2. pega Entrada e Saída para o fluxo de dados
            this.out = new ObjectOutputStream(this.requestSocket.getOutputStream());
            this.out.flush();
            this.in = new ObjectInputStream(this.requestSocket.getInputStream());
        } catch (Exception ex){
            if(this.out != null){
                this.out.close();
                this.out = null;
            }
            if(this.in != null){
                this.in.close();
                this.in = null;
            }
            if(this.requestSocket != null){
                this.requestSocket.close();
                this.requestSocket = null;
            }
//            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, null, ex);
            
           throw new Exception(ex.getCause());
        } 
    }

    public void enviarMensagem(String mensagem) {
        try {
            this.out.writeObject(mensagem);
            this.out.flush();
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    public String lerMensagem() {
        String mensagem = "";

        try {
            mensagem = (String) in.readObject();
        } catch (Exception ex) {
            log.error(ex);
        } finally {
            return mensagem;
        }
    }

    public void fecharConexao() {
        try {
            this.in.close();
            this.out.close();
            this.requestSocket.close();
        } catch (IOException ex) {
            log.error(ex);
        }
    }
}
