package exemplo.socket.cliente;

import exemplo.socket.servidor.Servidor;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Luidne
 */
public class Calculadora {

    public static void main(String[] args) {

        org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Servidor.class.getName()); // Logger do aplicativo
        PropertyConfigurator.configure("F:\\Luidne\\Documents\\NetBeansProjects\\exemplo-socket-2\\src\\log4j.properties");

        Cliente clientDNS = null;
        Cliente clientSomar = null;
        Cliente clientSubtrair = null;
        Cliente clientMultiplicar = null;
        Cliente clientDividir = null;
        String ipServidor = "";
        String ipDNS = "localhost:2005";
        try {
            clientDNS = new Cliente(ipDNS); // conecta no DNS
        } catch (Exception ex) {
            Logger.getLogger(Calculadora.class.getName()).log(Level.SEVERE, null, ex);
        }

        Scanner scanner = new Scanner(System.in);
        String teclado = "";
        String operandos = "";

        System.out.println("::::::::::::::::::: Calculadora Distribuída :::::::::::::::::::");
        do {
            System.out.println();
            System.out.println("1 - Somar");
            System.out.println("2 - Subtrair");
            System.out.println("3 - Multiplicar");
            System.out.println("4 - Dividir");
            System.out.println("sair - Sair");

            teclado = scanner.nextLine();
            switch (teclado) {
                case "1":
                    try {
                        clientDNS.enviarMensagem("somar"); // envia o nome do serviço desajado
                        ipServidor = clientDNS.lerMensagem(); // recebe o ip do serviço

                        if (clientSomar == null) {
                            clientSomar = new Cliente(ipServidor);
                        }

                        System.out.println("Digite os valores separados por \"+\"");
                        try {
                            operandos = scanner.nextLine();

                            clientSomar.enviarMensagem(operandos.trim().replace("+", " ")); // envia os valores para a operação
                            System.out.println(operandos + " = " + clientSomar.lerMensagem()); // recebe o resultado da operação
                        } catch (Exception ex) {
                            System.err.println("Houve algum erro. Por favor, tente de novo.");
                        }
                    } catch (Exception ex) {
                        if (clientSomar != null) {
                            clientSomar.fecharConexao();
                        }
                        System.err.println("Houve algum erro. Por favor, tente de novo.");
                    }

                    System.out.println();
                    break;
                case "2":
                    try {
                        clientDNS.enviarMensagem("subtrair"); // envia o nome do serviço desajado
                        ipServidor = clientDNS.lerMensagem(); // recebe o ip do serviço

                        if (clientSubtrair == null) {
                            clientSubtrair = new Cliente(ipServidor);
                        }
                        System.out.println("Digite os valores separados por \"-\"");
                        try {
                            operandos = scanner.nextLine();

                            clientSubtrair.enviarMensagem(operandos.trim().replace("-", " ")); // envia os valores para a operação
                            System.out.println(operandos + " = " + clientSubtrair.lerMensagem()); // recebe o resultado da operação


                        } catch (Exception ex) {
                            System.err.println("Houve algum erro. Por favor, tente de novo.");
                        }
                    } catch (Exception ex) {
                        if (clientSubtrair != null) {
                            clientSubtrair.fecharConexao();
                        }
                        System.err.println("Houve algum erro. Por favor, tente de novo.");
                    }

                    break;
                case "3":
                    try {
                        clientDNS.enviarMensagem("multiplicar"); // envia o nome do serviço desajado
                        ipServidor = clientDNS.lerMensagem(); // recebe o ip do serviço

                        if (clientMultiplicar == null) {
                            clientMultiplicar = new Cliente(ipServidor);
                        }

                        System.out.println("Digite os valores separados por \"*\"");
                        try {
                            operandos = scanner.nextLine();

                            clientMultiplicar.enviarMensagem(operandos.trim().replace("*", " ")); // envia os valores para a operação
                            System.out.println(operandos + " = " + clientMultiplicar.lerMensagem()); // recebe o resultado da operação
                        } catch (Exception ex) {
                            System.err.println("Houve algum erro. Por favor, tente de novo.");
                        }
                    } catch (Exception ex) {
                        if (clientMultiplicar != null) {
                            clientMultiplicar.fecharConexao();
                        }
                        System.err.println("Houve algum erro. Por favor, tente de novo.");
                    }

                    break;
                case "4":
                    try {
                        clientDNS.enviarMensagem("dividir"); // envia o nome do serviço desajado
                        ipServidor = clientDNS.lerMensagem(); // recebe o ip do serviço

                        if (clientDividir == null) {
                            clientDividir = new Cliente(ipServidor);
                        }

                        System.out.println("Digite os valores separados por \"/\"");
                        try {
                            operandos = scanner.nextLine();

                            clientDividir.enviarMensagem(operandos.trim().replace("/", " ")); // envia os valores para a operação
                            System.out.println(operandos + " = " + clientDividir.lerMensagem()); // recebe o resultado da operação
                        } catch (Exception ex) {
                            System.err.println("Houve algum erro. Por favor, tente de novo.");
                        }
                    } catch (Exception ex) {
                        if (clientDividir != null) {
                            clientDividir.fecharConexao();
                        }
                        System.err.println("Houve algum erro. Por favor, tente de novo.");
                    }

                    break;
            }
        } while (!teclado.equalsIgnoreCase("sair"));

        if (clientDNS != null) {
            clientDNS.enviarMensagem("sair"); // envia o comando para fechar a conexão
            clientDNS.fecharConexao();
        }

        if (clientSomar != null) {
            clientSomar.enviarMensagem("sair"); // envia o comando para fechar a conexão
            clientSomar.fecharConexao();
        }

        if (clientSubtrair != null) {
            clientSubtrair.enviarMensagem("sair"); // envia o comando para fechar a conexão
            clientSubtrair.fecharConexao();
        }

        if (clientMultiplicar != null) {
            clientMultiplicar.enviarMensagem("sair"); // envia o comando para fechar a conexão
            clientMultiplicar.fecharConexao();
        }

        if (clientDividir != null) {
            clientDividir.enviarMensagem("sair"); // envia o comando para fechar a conexão
            clientDividir.fecharConexao();
        }
    }
}
